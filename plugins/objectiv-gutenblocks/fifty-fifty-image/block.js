const { __ } = wp.i18n;
const {
	registerBlockType,
	Editable,
	MediaUploadButton,
	source: {
		attr,
		children
	}
} = wp.blocks;

registerBlockType( 'objectiv/obj-fifty-fifty-image', {
	title: __( 'Fifty Fifty Image' ),
	icon: 'index-card',
	category: 'layout',
	attributes: {
		mediaID_1: {
			type: 'number',
		},
		mediaURL_1: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
		mediaID_2: {
			type: 'number',
		},
		mediaURL_2: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
	},
	edit: props => {
		const attributes = props.attributes;
		const onSelectImage1 = media => {
			props.setAttributes( {
				mediaURL_1: media.url,
				mediaID_1: media.id,
			} );
		};

		const onSelectImage2 = media => {
			props.setAttributes({
				mediaURL_2: media.url,
				mediaID_2: media.id,
			});
		};

		return (
			<div className={ props.className }>
				<div className="fifty-fifty-image-1-editor">
					<MediaUploadButton
						buttonProps={
							{
								className: attributes.mediaID_1
									? 'image-button'
									: 'components-button button button-large',
							}
						}
						onSelect={ onSelectImage1 }
						type="image"
						value={ attributes.mediaID_1 }
						>
						{
							attributes.mediaID_1
								? <img src={ attributes.mediaURL_1 } />
								: __( 'Upload Left Side Image' )
						}
					</MediaUploadButton>
				</div>
				<div className="fifty-fifty-image-2-editor">
					<MediaUploadButton
						buttonProps={
							{
								className: attributes.mediaID_2
									? 'image-button'
									: 'components-button button button-large',
							}
						}
						onSelect={onSelectImage2}
						type="image"
						value={attributes.mediaID_2}
					>
						{
							attributes.mediaID_2
								? <img src={attributes.mediaURL_2} />
								: __('Upload Right Side Image')
						}
					</MediaUploadButton>
				</div>
			</div>
		);
	},
	save: props => {
		const {
			className,
			attributes: {
				mediaURL_1,
				mediaURL_2
			}
		} = props;
		return (
			<div className={ className }>
				{
					mediaURL_1 && (
						<div 
						className = "fifty-fifty-image-1"
						style = {
							{
								backgroundImage: "url(" + mediaURL_1 + ")"
							}
						} 
						>
						</div>
					)
				}
				{
					mediaURL_2 && (
						<div 
						className = "fifty-fifty-image-2"
						style = {
							{
								backgroundImage: "url(" + mediaURL_2 + ")"
							}
						} 
						>
						</div>
					)
				}
			</div>
		);
	}
} );
