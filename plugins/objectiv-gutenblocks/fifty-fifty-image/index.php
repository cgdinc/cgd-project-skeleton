<?php

defined( 'ABSPATH' ) || exit;

add_action( 'enqueue_block_editor_assets', 'objectiv_fifty_fifty_image_editor_assets' );

function objectiv_fifty_fifty_image_editor_assets() {
	wp_enqueue_script(
		'objectiv_fifty_fifty_assets',
		plugins_url( 'block.build.js', __FILE__ ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'underscore' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'block.build.js' )
	);
}

add_action( 'enqueue_block_assets', 'objectiv_fifty_fifty_image_assets' );

function objectiv_fifty_fifty_image_assets() {
	wp_enqueue_style(
		'objectiv_fifty_fifty_assets',
		plugins_url( 'style.css', __FILE__ ),
		array( 'wp-blocks' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);
}
