<?php

/**
 * Plugin Name: Objectiv GutenBlocks
 * Plugin URI: https://objectiv.co/
 * Description: This is a collection of Objectiv Gutenberg blocks.
 * Version: 0.0.1
 * Author: Objectiv
 *
 * @package objectiv-gutenblocks
 */
defined('ABSPATH') || exit;

// Include each block
include 'fifty-fifty-image/index.php';