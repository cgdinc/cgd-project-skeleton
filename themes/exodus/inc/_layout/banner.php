<?php
add_action( 'genesis_after_header', 'objectiv_page_banner', 15 );

function objectiv_page_banner() {

	// Get the name of the Page Template file.
	$template_file = get_post_meta( get_the_ID(), '_wp_page_template', true);

	if ( $template_file === "template-gutenberg.php" ) {
		// Do Nothing
	} elseif ( is_front_page() ) {
		echo get_template_part( 'partials/banner/home', 'banner' );
	} else {
		echo get_template_part( 'partials/banner/hero', 'banner' );
	}
}
