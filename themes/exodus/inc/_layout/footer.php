<?php

/**
 * Footer
 *
 */
function objectiv_footer() { ?>
	<div class="footer-creds">
		<div class="footer-left">
			<div>Copyright &copy; <?php echo date('Y') ?> COMPANY NAME, All rights reserved.</div>
		</div>
		<div class="footer-right">
			<?php if ( is_front_page() ) : ?>
				<div>Site by <a target="_blank" href="http://www.objectiv.co/">Objectiv</a></div>
			<?php else: ?>
				<div>Site by <a target="_blank" rel="nofollow" href="http://www.objectiv.co/">Objectiv</a></div>
			<?php endif; ?>
		</div>
	</div>
	<?php
}
