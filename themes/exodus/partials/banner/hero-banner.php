<?php

// Set up the height class
$height_class = decide_banner_height_class();

// Text Color and Overlay
$txt_color = get_field( 'text_color' );
$overlay = get_field( 'overlay' );

if ( empty( $overlay ) ) {
	$overlay = "dark-overlay";
}

if ( empty( $txt_color ) ) {
	$txt_color = "light-text";
}

// Figure the Title and Sub Title Out
$title = get_field( 'banner_title' );
if ( empty( $title ) ) {
    $title = decide_banner_title();
}
$subtitle = get_field( 'banner_sub_title' );

// Set up thumbnail
$bg_image_url = decide_banner_bg_img();

?>
<section class="page-banner-slider <?php echo $height_class ?>">
    <div class="page-banner__slide <?php echo $txt_color; ?>" style="background-image: url(<?php echo $bg_image_url ?>)">
        <div class="wrap">
            <div class="page-banner__content">
                <h1 class="page-banner__title"><?php echo $title; ?></h1>
                <?php if ( ! empty( $subtitle ) ): ?>
                    <h3 class="page-banner__subtitle"><?php echo $subtitle; ?></h3>
                <?php endif; ?>
            </div>
        </div>


        <?php if ( $overlay != 'none' ): ?>
            <div class="overlay <?php echo $overlay; ?>"></div>
        <?php endif; ?>


    </div>
</section>
