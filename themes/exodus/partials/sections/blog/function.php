<?php

return (object) array(
	'acf_name'  => 'blog_section',
	'options'   => (object) array(
		'func'      => function ($padding_classes = '') {

            $p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
			$sb_loc = "$p_loc/blog";

			$start = "$sb_loc/start.php";
			$end = "$sb_loc/end.php";
			$item = "$sb_loc/item.php";

			$title = get_sub_field( 'title' );
			$number_posts = '3';
			$class = 'three-posts';

			$args = array(
				'post_type' => 'post',
				'posts_per_page' => $number_posts,
			);
			$loop = new WP_Query($args);
			
			require($item);

		},
		'has_padding'   => true
	)
);
