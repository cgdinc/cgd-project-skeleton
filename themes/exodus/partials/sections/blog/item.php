<?php if ( $loop->have_posts() ): ?>
    <section class="page-flexible-section page-section-stories <?php echo $padding_classes; ?> <?php echo $class; ?>">
    	<div class="wrap">
		<?php obj_section_header($title); ?>
    		<div class="stories">
            	<?php while ( $loop->have_posts() ): $loop->the_post(); ?>
            		<div class="story">

                        <?php if ( has_post_thumbnail() ): ?>
                            <?php the_post_thumbnail('objectiv_featured'); ?>
                        <?php else : ?>
                            <?php 
                            $default_bg_image_id = get_field('default_banner_image', 'options');
                            $default_image = wp_get_attachment_image($default_bg_image_id['ID'], 'objectiv_featured');
                            ?>
                            <?php if( ! empty( $default_image ) ) : ?>
                                <?php echo $default_image; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        <div class="story-blurb">
                            <h3 class="story-title"><?php the_title(); ?></h3>
                            <div class="story-blurb-content">
                                <?php echo objectiv_get_short_description( get_the_ID(), 22 ); ?>
                            </div>
                            <span class="button">
                                <a href="<?php the_permalink(); ?>">Read More</a>
                            </span>
                        </div>
            		</div>
            	<?php endwhile; ?>
        	<?php wp_reset_postdata(); ?>
        </div>
    </div>
</section>
<?php endif; ?>
