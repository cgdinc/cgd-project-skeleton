<?php

return (object) array(
	'acf_name'  => 'image_grid_section',
	'options'   => (object) array(
	'func'      => function ($padding_classes = '') {
		$p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
		$fcta_loc = "$p_loc/cta-image-grid";
		$item = "$fcta_loc/item.php";

		$title = get_sub_field( 'title' );
		$description = get_sub_field( 'description' );
		$image_with_link = get_sub_field( 'image_with_link' );
		$open_in_new = get_sub_field( 'link_behavior' );
		$button_details = get_sub_field( 'button_link' );


		require($item);
	},
	'has_padding'   => true
	)
);
