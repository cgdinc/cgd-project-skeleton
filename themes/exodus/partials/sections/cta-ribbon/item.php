<?php if ( ! empty($first_text) || ! empty($second_text) && ! empty($url) ) : ?>
	<section class="page-flexible-section <?php echo $padding_classes; ?>" >
		<div class="ribbon-cta-section">
			<a target="<?php echo $link_details['target'] ?>" href="<?php echo $url ?>" class="ribbon-content">
				<div class="wrap">
					<span class="ribbon-text">
						<?php if ( ! empty( $first_text ) ) : ?>
							<span class="top-text"><?php echo $first_text ?></span>
						<?php endif; ?>

						<?php if ( ! empty( $second_text ) ) : ?>
							<h2 class="bottom-text"><?php echo $second_text ?></h2>
						<?php endif; ?>
					</span>
					<span class="arrow-link">
						<span class="inner-arrow-link">
							<?php echo get_svg_icon( 'ribbon-cta-arrow', '', 18, 18 ); ?>
						</span>
					</span>
				</div>
			</a>

		</div>
	</section>
<?php endif; ?>
