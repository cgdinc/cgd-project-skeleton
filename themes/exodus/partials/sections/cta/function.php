<?php

return (object) array(
	'acf_name'  => 'cta_section',
	'options'   => (object) array(
	'func'      => function ($padding_classes = '') {
		$p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
		$fcta_loc = "$p_loc/cta";
		$item = "$fcta_loc/item.php";

		$bg_img = get_sub_field('background_image')['url'];
		$disable_overlay = get_sub_field('image_overlay');
		$title = get_sub_field('title');
		$sub_title = get_sub_field('sub_title');
		$info_blurb = get_sub_field('info_blurb');
		$bg_type = get_sub_field('background_type');
		$button_details = get_sub_field('button_details');

		if ( $bg_type == "color" ) {
			$bg_img = null;
		}

		require($item);
	},
	'has_padding'   => false
	)
);
