<section class="cta-section page-flexible-section <?php echo $padding_classes; ?>" style="background-image: url(<?php echo $bg_img ?>);">
	<div class="wrap">
		<div class="cta-content">

			<?php if ( ! empty( $title ) ) : ?>
				<h2 class="section-title"><?php echo $title ?></h2>
			<?php endif; ?>

			<?php if ( ! empty( $sub_title ) ) : ?>
				<h6 class="section-sub-title"><?php echo $sub_title ?></h6>
			<?php endif; ?>

			<?php if ( ! empty( $info_blurb ) ) : ?>
				<h4 class="section-post-title"><?php echo $info_blurb ?></h4>
			<?php endif; ?>

			<?php if ( ! empty( $button_details ) ) : ?>
				<?php echo objectiv_link_button( $button_details ) ?>
			<?php endif; ?>

		</div>

		<?php if ( ! $disable_overlay  ) : ?>
			<div class="cta-overlay">
			</div>
		<?php endif; ?>

	</div>
</section>
