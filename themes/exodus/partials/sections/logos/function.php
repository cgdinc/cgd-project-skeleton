<?php

return (object) array(
	'acf_name'  => 'logos_section',
	'options'   => (object) array(
	'func'      => function ($padding_classes = '') {
		$p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
		$fcta_loc = "$p_loc/logos";
		$item = "$fcta_loc/item.php";

		$bg_color = get_sub_field('background_color');
		$title = get_sub_field('section_title');
		$blurb = get_sub_field('section_blurb');
		$logos = get_sub_field('logos');

		$logos_number = count($logos);

		require($item);
	},
	'has_padding'   => false
	)
);
