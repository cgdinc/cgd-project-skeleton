<section class="logos-section page-flexible-section <?php echo $padding_classes; ?> color <?php echo $bg_color; ?>">
	<div class="wrap">
		<div class="logos-content light">

			<div class="left-side">
				<?php if ( ! empty( $title ) ) : ?>
					<h2 class="section-title"><?php echo $title ?></h2>
				<?php endif; ?>

				<?php if ( ! empty( $blurb ) ) : ?>
					<h4 class="section-post-title"><?php echo $blurb ?></h4>
				<?php endif; ?>
			</div>

			<div class="right-side">
				<div class="logos one2grid">
					<?php foreach ( $logos as $l ) :
						$logo_img = $l['logo_image'];
						?>
						<div class="logo-block">
							<img class="logo" src="<?php echo $logo_img ?>" alt="<?php echo $logo['alt'] ?>" />
						</div>
					<?php endforeach; ?>
				</div>
			</div>

		</div>

	</div>
</section>
