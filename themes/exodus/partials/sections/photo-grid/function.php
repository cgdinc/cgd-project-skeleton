<?php

return (object) array(
	'acf_name'  => 'photo_grid_section',
	'options'   => (object) array(
	'func'      => function ($padding_classes = '') {
		$p_loc = FlexibleContentSectionUtility::getSectionsDirectory();
		$sb_loc = "$p_loc/photo-grid";
		$item = "$sb_loc/item.php";

		$photos = get_sub_field('photo_section_photos');

		require($item);
	},
	'has_padding'   => true
	)
);
