
<?php if ( ! empty( $photos ) ) : ?>

	<section class="photo-grid-section page-flexible-section <?php echo $padding_classes; ?>">
		<div class="wrap wrap_thin">
			<div class="photo-grid-grid">
				<?php foreach ( $photos as $photo ) : ?>
					<a href="<?php echo $photo['image']['sizes']['large'] ?>" rel="gallery" class="section gallery">
						<div class="grid-section-image" style="background-image: url(<?php echo $photo['image']['sizes']['large'] ?>)">
						</div>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</section>

<?php endif; ?>
